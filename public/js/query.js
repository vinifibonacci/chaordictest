function parseResponse(response)
{
	
	
	response.data.recommendation.forEach(function(item)
	{
		createSliderItem(item);
	});
}

function createSliderItem(item)
{
	var li 	= document.createElement("li");
	var ul 	= document.getElementById("slider-list");
	
	var html	= 	'<a href="http:'+item.detailUrl+'">';
		html 	+= 	'<div class="product-container" data-bid="'+item.businessId+'">';
		html 	+= 	'<div class="thumb">';
		html 	+= 	'<img src="http:'+item.imageName+'">'
		html 	+= 	'</div>'
		html 	+= 	'<div class="product-info">'
		html 	+= 	'<span class="name">'+item.name.replace(/^(.{70}[^\s]*).*/, "$1")+'...</span>';
		
		if(item.oldPrice != null)
		{
			html 	+= 	'<span class="old-price">De: '+item.oldPrice+'</span>';
		}
		
		html 	+= 	'<span class="price">Por: <strong>'+item.price+'</strong></span>';
		html 	+= 	'<span class="price installment">'+item.productInfo.paymentConditions+'<br>sem juros</span>';
		html 	+= 	'</div>';
		html 	+= 	'</div>';
		html 	+= 	'</a>';
	
	li.innerHTML = html;
	
	ul.appendChild(li);
}

var xhr = new XMLHttpRequest();
xhr.open('GET', 'js/challenge.json');
xhr.onload = function() {
    if (xhr.status === 200) {
    	
    	var jsonResponse = JSON.parse(xhr.responseText);
    	
    	parseResponse(jsonResponse);
    	
    }
    else {
        alert('Request failed.  Returned status of ' + xhr.status);
    }
};
xhr.send();